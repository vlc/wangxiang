/*****************************************************************************
 * emphisize.c: to lift the high-frequency filter
 *****************************************************************************
 * Copyright (C) 2001, 2006 the VideoLAN team
 * $Id$
 *
 * Authors: richard wang <wangxiang68@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*
 * TODO:
 *
 * 
 * Because in the speech signal processing theory, the frequency of 
 * 6kz-8kz should be emphsized y(n) = x(n)- a*x(n-1) equal to H(Z) = 1- a/Z */


/*****************************************************************************
 * Preamble
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <errno.h>                                                 /* ENOMEM */
#include <ctype.h>
#include <signal.h>

#include <math.h>


#include <vlc_common.h>
#include <vlc_plugin.h>

#include <vlc_aout.h>

/*****************************************************************************
 * Local prototypes
 *****************************************************************************/

static int  Open     ( vlc_object_t * );
static void DoWork   ( aout_instance_t * , aout_filter_t *,
                aout_buffer_t * , aout_buffer_t *);

/*****************************************************************************
 * Module descriptor
 *****************************************************************************/
#define BUFF_TEXT N_("Number of audio buffers" )
#define BUFF_LONGTEXT N_("the frequency of  6kz-8kz should be emphsized y(n) = 1-0.97*y(n-1) ")

vlc_module_begin();
    set_description( N_("Raise the voice of speaker") );
    set_shortname( N_("Raise the voice of speaker") );
    set_capability( "audio filter", 0 );
    set_callbacks( Open, Close );
vlc_module_end();

/*****************************************************************************
 * Open: initialize and create stuff
 *****************************************************************************/
static int Open( vlc_object_t *p_this )
{
    aout_filter_t *p_filter = (aout_filter_t*)p_this;
    int i_channels;

    if( p_filter->input.i_format != VLC_FOURCC('f','l','3','2' ) ||
        p_filter->output.i_format != VLC_FOURCC('f','l','3','2') )
    {
        b_fit = false;
        p_filter->input.i_format = VLC_FOURCC('f','l','3','2');
        p_filter->output.i_format = VLC_FOURCC('f','l','3','2');
        msg_Warn( p_filter, "bad input or output format" );
    }

    if ( !AOUT_FMTS_SIMILAR( &p_filter->input, &p_filter->output ) )
    {
        b_fit = false;
        memcpy( &p_filter->output, &p_filter->input,
                sizeof(audio_sample_format_t) );
        msg_Warn( p_filter, "input and output formats are not similar" );
    }

    p_filter->pf_do_work = DoWork;
    p_filter->b_in_place = true;

    i_channels = aout_FormatNbChannels( &p_filter->input );

    return VLC_SUCCESS;
}

/*****************************************************************************
 * DoWork : normalizes and sends a buffer
 *****************************************************************************/
 static void DoWork( aout_instance_t *p_aout, aout_filter_t *p_filter,
                     aout_buffer_t *p_in_buf, aout_buffer_t *p_out_buf )
{
    int i, i_chan;

    int i_samples = p_in_buf->i_nb_samples;
    int i_channels = aout_FormatNbChannels( &p_filter->input );
    float *p_out = (float*)p_out_buf->p_buffer;
    float *p_in =  (float*)p_in_buf->p_buffer;

    struct aout_filter_sys_t *p_sys = p_filter->p_sys;

    p_out_buf->i_nb_samples = p_in_buf->i_nb_samples;
    p_out_buf->i_nb_bytes = p_in_buf->i_nb_bytes;

/*Apply Pre-emphasize*/	
	for (i = i_samples-1; i > 0; i-- )
	{
		for (i_chan = 0; i_chan < i_channels; i++)
		{
			p_out[i+i_chan] = p_in[i+i_chan] - p_in[i-1+i_chan]*0.97;
		}
	}

	for (i_chan = 0; i_chan < i_channels; i++)
	{
		p_out[i_chan] = 0.03*p_in[i_chan];
	}
	
    return;
}